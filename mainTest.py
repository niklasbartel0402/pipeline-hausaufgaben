import unittest
from main import Greeter

class MyTestCase(unittest.TestCase):
    def test_default_greeting_set(self):
        greeter = Greeter()
        self.assertEqual(greeter.message, 'Hello world!')

    def test_something_else(self):
        greeter = Greeter()
        self.assertEqual(greeter.something, 'something else')

if __name__ == '__main__':
    unittest.main()